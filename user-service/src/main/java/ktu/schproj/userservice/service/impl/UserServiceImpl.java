package ktu.schproj.userservice.service.impl;

import ktu.schproj.userservice.data.User;
import ktu.schproj.userservice.data.UserDto;
import ktu.schproj.userservice.mapper.UserMapper;
import ktu.schproj.userservice.repository.UserRepository;
import ktu.schproj.userservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

  @Autowired private UserRepository userRepository;
  @Autowired private ModelMapper modelMapper;
  @Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Override
  public UserDto createUser(UserDto userDto) {
    userDto.setEncryptedPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
    User entity = UserMapper.DtoToEntity(userDto);
    userRepository.save(entity);
    return modelMapper.map(entity, UserDto.class);
  }

  @Override
  public UserDto updateUser(String id, UserDto userDto) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (!optionalUser.isPresent()) {
      log.error("User with given id is not found in our database.");
      throw new IllegalArgumentException();
    }
    User userdb = optionalUser.get();
    userdb = userRepository.save(UserMapper.updateUserEntity(userdb, userDto));
    return modelMapper.map(userdb, UserDto.class);
  }

  @Override
  public UserDto getUserById(String id) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (!optionalUser.isPresent()) {
      log.error("User with given id is not found in our database.");
      throw new IllegalArgumentException();
    }
    return modelMapper.map(optionalUser.get(), UserDto.class);
  }

  @Override
  public void deleteUserById(String id) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (!optionalUser.isPresent()) {
      log.error("User with given id is not found in our database.");
      throw new IllegalArgumentException();
    }
    userRepository.deleteById(id);
  }
}
