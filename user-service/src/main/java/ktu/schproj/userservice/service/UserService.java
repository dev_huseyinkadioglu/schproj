package ktu.schproj.userservice.service;

import ktu.schproj.userservice.data.UserDto;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

  UserDto createUser(UserDto userDto);

  UserDto updateUser(String id, UserDto userRequestModel);

  UserDto getUserById(String id);

  void deleteUserById(String id);
}
