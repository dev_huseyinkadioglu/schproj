package ktu.schproj.userservice.api;

import ktu.schproj.userservice.data.UserDto;
import ktu.schproj.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired private UserService userService;

  @PostMapping("/create")
  public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userRequestModel) {
    return ResponseEntity.ok(userService.createUser(userRequestModel));
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<UserDto> updateUser(
      @PathVariable("id") String id, @Valid @RequestBody UserDto userDto) {
    return ResponseEntity.ok(userService.updateUser(id, userDto));
  }

  @GetMapping("/{id}")
  public ResponseEntity<UserDto> getUserById(@PathVariable("id") String id) {
    return ResponseEntity.ok(userService.getUserById(id));
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity deleteUserById(@PathVariable("id") String id) {
    userService.deleteUserById(id);
    return new ResponseEntity(HttpStatus.OK);
  }
}
