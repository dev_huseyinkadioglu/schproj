package ktu.schproj.userservice.mapper;

import ktu.schproj.userservice.data.User;
import ktu.schproj.userservice.data.UserDto;

public class UserMapper {
  public static User updateUserEntity(User user, UserDto userDto) {
    user.setEmail(userDto.getEmail());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setPassword(userDto.getPassword());
    return user;
  }

  public static User DtoToEntity(UserDto dto) {
    User user = new User();
    user.setPassword(dto.getEncryptedPassword());
    user.setFirstName(dto.getFirstName());
    user.setLastName(dto.getLastName());
    user.setEmail(dto.getEmail());
    return user;
  }
}
